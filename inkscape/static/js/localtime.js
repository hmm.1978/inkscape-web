/*
 * Convert datetimes to the local or time until based on tzinfo
 */
var dt_settings = {
    weekday: 'short',
    month: 'short',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    timeZoneName: 'short',
};
var utc_settings = {
    timeZone: 'UTC',
    year: 'numeric',
    month: 'short',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    timeZoneName: 'short',
}

$(document).ready(function() {
    $('span[data-event]').each(function() {
        $(this).click(function() {
            var elem = $(this);
            var date = new Date(elem.data('event'));
            if(elem.hasClass('btn-default')) {
                var text = date.toLocaleDateString(undefined, dt_settings);
            } else {
                var text = date.toLocaleDateString("en", utc_settings);
            }
            elem.text(text);
            elem.toggleClass('btn-default');
            elem.toggleClass('btn-primary');
        }).click();
    });
});
